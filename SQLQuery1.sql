/*1. �������� ������� ��� ����������� ���������� ������ �� ����������:
 ��������� 3 ����� ������ ���������� ��������;
 ���� �������� �������;
 ���������� ��� �������;*/
 CREATE PROCEDURE spCreateCreditTableOne
 AS
 CREATE TABLE CreditTableOne
(
	CreditId CHAR(3) PRIMARY KEY,
	Birthday DATE NOT NULL,
    ClientId INT NOT NULL
);
 GO

 /*2. ���������� ������� (1) ����� ������� ������*/
 CREATE PROCEDURE spFillCreditTableOne
 AS
 INSERT INTO CreditTableOne VALUES('123','20000909',1)
 GO

 /*3. �������� ������� (2) �� ���������� ���������� ������� (1).*/
 CREATE PROCEDURE spCreateCreditTableTwo
 AS
 CREATE TABLE CreditTableTwo
(
	CreditId CHAR(3) PRIMARY KEY,
	Birthday DATE NOT NULL,
    ClientId INT NOT NULL
);
 GO

 /*4. ���������� ������� (2) ��� ���������� � �������� (� ������ �����) ������� �� �������
������ �������*/
CREATE PROCEDURE spAlterCreditTableTwo
 AS
	ALTER TABLE CreditTableTwo
		ADD Age INT NOT NULL
 GO

/*5.�������� � ������� (2) ������ ������� (1) � ������ ���������� � ��������.*/ 
CREATE PROCEDURE spFillCreditTableTwo
 AS
 DECLARE @TODAY DATE;
 DECLARE @DIFF DATE;
 SET @TODAY = CONVERT(date,GETDATE())
 INSERT INTO CreditTableTwo(CreditId, Birthday, ClientId, Age)
 SELECT CreditId, Birthday, ClientId, DATEDIFF(YEAR,Birthday,@TODAY)
 FROM CreditTableOne
 GO

/*6. ���������� ������� 2-�� ��������� �������:
6.1.���� ��������, ������������ � ������� (1), �� �� ������������ � ������� (2);
*/
CREATE PROCEDURE spOneNotTwo
AS
SELECT o.ClientID
FROM CreditTableOne AS o
LEFT OUTER JOIN CreditTableTwo AS t ON o.ClientID = t.ClientID
WHERE t.ClientID is NULL
GO

CREATE PROCEDURE spOneNotTwoAlt
AS
SELECT o.ClientID
FROM CreditTableOne AS o
WHERE o.ClientID NOT IN(SELECT t.ClientID FROM CreditTableTwo AS t)
GO

/*6.2.����������� (��� ����������) ������ �������� �� ������ (1) � (2);*/
CREATE PROCEDURE spUniqueClients
AS
SELECT o.ClientID FROM CreditTableOne AS o
UNION SELECT t.ClientID FROM CreditTableTwo AS t
GO

CREATE PROCEDURE spUniqueClientsAlt
AS
		  SELECT DISTINCT o.ClientID FROM CreditTableOne AS o
UNION ALL SELECT DISTINCT t.ClientID
		  FROM CreditTableTwo AS  t
		  WHERE t.ClientID NOT IN(
									SELECT DISTINCT tb.ClientID
									FROM CreditTableTwo AS tb
									)
GO

/*7. ������� ��������, � ������� ������������� ������ 5 000 ������. �� ������ �������
����� ���� ��������� ���������.
���������:
- ������������� ������� (id_clients)
- ������������� �������� (Id_loans)
- ������� �� ������� (amount)
- ���� ������� (term)*/
 CREATE PROCEDURE spCreateCreditTable
 AS
 CREATE TABLE CreditTable
(
	id_clients INT NOT NULL,
	Id_loans INT PRIMARY KEY NOT NULL,
	amount INT NOT NULL,
	term DATE NOT NULL,
);
 GO

 CREATE PROCEDURE spTotalAmountMoreThen5000
 AS
	SELECT id_clients, SUM(amount) AS total_credit FROM CreditTable
	GROUP BY id_clients
	HAVING SUM(amount)>5000
 GO

/*8. �� ������� ����� ���� ������� �������� �� ������� ������������� �� ���������
�������� ����. ��������� �������:
- �������� ���� (report_date)
- ������������� �������� (Id_loans)
- ������� ������������� (amount)
- ������� (product)
��������, ����������� ������ �������*/
CREATE PROCEDURE spCreateDateCreditTable
AS
CREATE TABLE DateCreditTable
(
	report_date DATE NOT NULL,
	Id_loans INT NOT NULL,
	amount INT NOT NULL,
	product VARCHAR(20) NOT NULL,
	PRIMARY KEY (report_date,Id_loans)
);
Go

CREATE PROCEDURE spCreditRatioOnLastDate
AS
DECLARE @SUM INT;
SET @SUM = (SELECT SUM(o.amount) FROM DateCreditTable AS o
			WHERE o.report_date IN 
				(SELECT MAX(tb.report_date)
				 FROM DateCreditTable AS tb
				 WHERE o.Id_loans=tb.Id_loans))

SELECT o.Id_loans, o.amount/@SUM AS total_amount FROM DateCreditTable AS o
/**/
WHERE o.report_date IN 
	(SELECT MAX(tb.report_date)
	 FROM DateCreditTable AS tb
	 WHERE o.Id_loans=tb.Id_loans)
	 /*���, ��������, ������ ���� ���-�� ���������
WHERE (o.Id_loans, o.report_date IN 
	(SELECT tb.Id_loans, MAX(tb.report_date)
	 FROM DateCreditTable tb
	 GROUP BY tb.id_clients)
	 �� �� ����������*/
GO

EXEC spCreateCreditTableOne;
EXEC spFillCreditTableOne;
EXEC spCreateCreditTableTwo;
EXEC spAlterCreditTableTwo;
EXEC spFillCreditTableTwo;
EXEC spOneNotTwo;
EXEC spOneNotTwoAlt;
EXEC spUniqueClients;
EXEC spUniqueClientsAlt;
EXEC spCreateCreditTable;
EXEC spTotalAmountMoreThen5000;
EXEC spCreateDateCreditTable;
EXEC spCreditRatioOnLastDate;
